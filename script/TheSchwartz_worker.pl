#!/usr/bin/env perl

use FindBin;
use lib "$FindBin::Bin/../lib";

use Catapulse::XUtils qw/theschwartz config home/;

use Parallel::Prefork;
use Proc::PID::File;

my $piddir = home() . "/var/";
my $config = config();

# If already running, then exit # 
if ( Proc::PID::File->running( { dir => $piddir } ) ) {
    exit(0);
}

my $client = theschwartz();

my $pm = Parallel::Prefork->new({
                                 max_workers  => $config->{TheSchwartz}->{max_workers},
                                 trap_signals => {
                                                  TERM => 'TERM',
                                                  HUP  => 'TERM',
                                                 }
                                });

my $workers = $config->{TheSchwartz}->{workers};
while ($pm->signal_received ne 'TERM') {
    $pm->start and next;

    foreach my $worker (keys %$workers) {
        my $path = $worker . ".pm";
        $path =~ s{::}{/}g;
        eval { require $path };
        if ($@) {
            die "Can not load $worker : $@";
        } else {
            $client->can_do($worker);
        }
    }
    $client->work( $config->{TheSchwartz}->{delay} );

    $pm->finish;
}

foreach my $pid (sort keys %{$pm->{worker_pids}}) {
    print "exit TheSchwartz child pid=$pid\n";
}

$pm->wait_all_children();




