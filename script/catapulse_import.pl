#!/usr/bin/env perl

=head1 NAME

catapulse_import.pl - import file in catapulse

=head1 AUTHOR

Daniel Brosseau <dab@catapulse.org>

=head1 COPYRIGHT

2018 Daniel Brosseau <dab@catapulse.org>

=cut


use strict;
use warnings;
use FindBin '$Bin';
use lib "$Bin/../lib";
use Catapulse::XUtils qw(config schema upload_dir theschwartz);
use Getopt::Long;
use File::Basename qw(basename);
use File::Path qw(make_path);
use File::Copy;

my $default_user   = $ENV{USER} || 'unknown';
my $default_dir    = '.';
my $default_url    = '/';
my $default_filter = '*.org';

my ($help, $dir, $url, $user, $filter);

GetOptions(
           'help'             => \$help,
           'dir:s'            => \$dir,
           'url:s'            => \$url,
           'user:s'           => \$user,
           'filter:s'         => \$filter,
          );

$dir    ||= $default_dir;
$url    ||= $default_url;
$user   ||= $default_user;
$filter ||= $default_filter;


if ($help) {
  print <<"EOF";

catapulse_import.pl ...

This script import files in catapulse.

If it's a org file it translate it in Html.
    
Accepts the following options:

  --dir              Directory to find files   ( default : '$default_dir' )
  --url              Url to published          ( default : '$default_url' )
  --user             Creator                   ( default : \$ENV{USER} )
  --filter           To select files to import ( default : '$default_filter' )

 Ex: 
 cd share/doc/
 $0 --dir examples

 IMPORTANT : Catapulse muse be started to import org file
EOF
  exit;
}

print "Are you sure you want to import $dir/$filter to $url ? [Y/n] :";
my $an = <STDIN>;
chomp ($an);
exit if ( $an =~ /n/i );


my $schema = schema();
my $upload_dir = upload_dir();
my @files = glob($dir . '/' . $filter);

build_pages(\@files);


sub build_pages{
  my $files = shift;
  
  foreach my $file ( @$files) {

    my $url_file = $url . '/' . $file. '/';
    $url_file =~ s|//|/|g;
    $url_file =~ s|/\./|/|g;

    print "Build page $url_file\n";
    # Returns all objects representing the path
    my $pages  = $schema->resultset('Page')->build_pages_from_path( {path => $url_file, type => 3}  );
    my $person = $schema->resultset('User')->search( { name => $user}  )->single;

    # the last node of page
    my $page = $$pages[-1];

    my $p = {
             path       => $url_file,
             template   => 'Main',
             title      => basename($url_file),
             type       => 'orgmode',
             ops_to_access => [ 'view_Page'],
            };

    my $upload_dir_page = "$upload_dir/" . $url_file;
    if ( ! -e "$upload_dir_page" ) {
      print "make_path($upload_dir_page)\n";
      make_path($upload_dir_page);
    }

    copy($file, $upload_dir_page) or die "Copy failed: $!";

    my $client = theschwartz();
    my $args = {
                orgfile => "$upload_dir_page/" .  basename($file),
                user_id => $person->id,
                page_id => $page->id,
                in_db   => 1,
               };

    $client->insert('Catapulse::TheSchwartz::Worker::Emacs', $args);
  }
}
