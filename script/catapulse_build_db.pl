#!/usr/bin/env perl

=head1 NAME

catapulse_build_db.pl - Build Database 

=head1 AUTHOR

Daniel Brosseau <dab@catapulse.org>

=head1 COPYRIGHT

2018 Daniel Brosseau <dab@catapulse.org>

=cut

use strict;
use warnings;
use FindBin '$Bin';
use lib "$Bin/../lib";
use Catapulse::XUtils qw(config dsn schema);

my $config = config();
my $schema = schema();
my $dsn    = dsn();

# Check if database is already deployed by
# examining if the table Person exists and has a record.
eval { $schema->resultset('Catapulse::Schema::Result::Person')->count };
if (!$@) {
  die "You have already deployed your database\n";
}

print <<"EOF";

Creating a new cms ...

  dsn:            $dsn
  cms name:       $config->{name}

EOF

print "Deploying schema to $dsn\n";
$schema->deploy;

