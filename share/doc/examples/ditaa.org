This page is a modified version of https://github.com/dfeich/org-babel-examples

* Documentation and links


  - [[http://ditaa.sourceforge.net][Ditaa on sourceforge]]
  - Drawin is mostly done in Emacs *Artist mode*
    - when using C-c ' =org-edit-special= the edit buffer is placed in
      artist mode
    - [[help:artist-mode][emacs help page on artist mode]]
    - [[http://www.cinsk.org/emacs/emacs-artist.html][nice screencast by Seong-Kook Shin]]
    - when drawing using the keys
      - *polyline*: =return= will begin a line, =C-u return= will end
	the polyline
      - *cut*: =C-c C-a C-k= for entering cut mode. Hit =return= to start
	opening a rectangle. Hit =Return= again for cutting the rectangle.
      - *paste*: =C-c C-a M-w= for entering past mode. Hit =return= for
	pasting.


* Ditaa Examples

** GRO Modules

#+BEGIN_SRC ditaa :file gro-modules.png :cmdline -r -s 0.8

                                        GRO Architecture
   ----------------------------------------------------------------------------------------------------- 
                                          Top Modules
    /----------\                                                                                         
    |    GUI   |                                                                                         
    | cRED     |                                                                                         
    +----------+                                                                                         
   ---------=----------------------------------------------------------------------------------=--------
    +----------+ +-----------\/-----------------\/--------+ 
    | UCI      | |LUCI-SHARED|| SDK-LIBC-SCRIPT || DNSMASQ| 
    | c516     | | cBLU      || cBLU            ||   cBLU | 
    +----------+ +-----------/\-----------------/\--------+ 
   ----------------------------------------------------------------------------------------------------- 
                                          Lower Modules
    +-----------------------------------------------------------------------------------------------+
    |                                         netfilter                                             |
    |  +------------------+                                                                         |
    |  | Access Control   |                                                                         |
    |  |   c277           |                                                                         |
    |  +------------------+                                                                         |
    |                                           cA7A                                                |
    +-----------------------------------------------------------------------------------------------+
    +-----------+  +--------------+ 
    |SDK-LIBC   |  | Busybox      |
    |cBLU       |  | Driver  cPNK |
    +-----------+  +--------------+
                                                                                                         


#+END_SRC
#+RESULTS:
[[file:gro-modules.png]]

** Feature Modules
#+BEGIN_SRC ditaa :file feature-modules.png :cmdline -r -s 0.8

          +-------------+
          |   UCI       |
          |   c516      |
          +------+------+
                 | config get
                 v                   /----------------------------------------------------\
          +-------------+            | - Translate configratuion items from UCI to uci    |
          | LUCI-SHARED |............| - Provide luci script to bring up feature          |
          |    cBLU     |            | - Provide Ada scripts                              |
          +------+------+            \----------------------------------------------------/
                 |
                 v
         +----------------+
         | Openwrt config |
         | cGRE           |
         +-------+--------+
                 |
                 v
        +-------------------+         +------------+
        |  SDK-LIBC-SCRIPT  +-------->|  DNSMASQ   |
        |     cBLU          |         |   cBLU     |
        +---------+---------+         +------+-----+
                  |                          | (Ada configuration)
                  |      /-------------------+
                  |      |
                  v      v
        +-------------------+
        |     SDK-LIBC      |
        |       cBLU        |
        +-------------------+

          
                
#+END_SRC
#+RESULTS:
[[file:feature-modules.png]]

#+begin_src ditaa :file test_ditaa.png :cmdline -r -s 1.0

    +-----------+        +---------+
    |    PLC    |        |         |
    |  Network  +<------>+   PLC   +<---=---------+
    |    cRED   |        |  c707   |              |
    +-----------+        +----+----+              |
                              ^                   |
                              |                   |
                              |  +----------------|-----------------+
                              |  |                |                 |
                              v  v                v                 v
      +----------+       +----+--+--+      +-------+---+      +-----+-----+       Windows clients
      |          |       |          |      |           |      |           |      +----+      +----+
      | Database +<----->+  Shared  +<---->+ Executive +<-=-->+ Operator  +<---->|cYEL| . . .|cYEL|
      |   c707   |       |  Memory  |      |   c707    |      | Server    |      |    |      |    |
      +--+----+--+       |{d} cGRE  |      +------+----+      |   c707    |      +----+      +----+
         ^    ^          +----------+             ^           +-------+---+
         |    |                                   |
         |    +--------=--------------------------+
         v
+--------+--------+
|                 |
| Millwide System |            -------- Data ---------
| cBLU            |            --=----- Signals ---=--
+-----------------+

#+END_SRC


#+BEGIN_SRC ditaa :file ditaa-concept.png  :cmdline -r -s 1.2

      +--------+   +-------+    +-------+
      |        | --+ ditaa +--> |       |
      |  Text  |   +-------+    |diagram|
      |Document|   |!magic!|    |       |
      |     {d}|   |       |    |       |
      +---+----+   +-------+    +-------+
          :                         ^
          |       Lots of work      |
          +-------------------------+
#+END_SRC


#+BEGIN_SRC ditaa :file ditaatest1.png :cmdline -s 0.8

    +----------------------+         +------------------+----+
    | This is a box   |    +---------+  and this is     |    |
    |                 |    |         |  another         |    |
    |                 |    |         |                  |    |
    |                 |    |         |                  |    |            +-------+
    +-------+--------------+         +------------------+----+            |       |
            |                                                             |       |
            |                                                             | {mo}  |
            :           /------------------------------\      |      ^	  +-------+
            |           |                              |      |      |
            |           |   and yet another, but       |      |      |
            +---------->+   with round corners         |      |      |    +-------+
                        |   o list item 1              |      |      |    |       |
                        |   o list item 2              |      |      |    |       |
                        \------------------------------/      V      |    | {tr}  |       
                                                                          +-------+       
                                                                                          
      +----------+       +--------------+       +---------------+    +-------+    +-------+
      | Storage  |       | Document     |       | Input/Output  |    |       |    |       |
      |          |       |              |       |               |    |       |    |       |
      | cGRE     +-------+ cYEL         +---=---+ cRED          |    | {o}   |    | {c}   |
      |          |       |              |       |               |    +-------+    +-------+
      | {s}      |       | {d}          |       | {io}          |
      +----------+       +--------------+       +---------------+


#+END_SRC

#+BEGIN_SRC ditaa :file polyline.png :cmdline -r -s 0.8
                                                               
    +---+    +----+    +---+                                   
    |   |    |    |    |   |          +--+  +--+               
    |   +----+    +----+   |          |  |  |  |               
    |                      |     -----+  +--+  |       +------*
    |      some shape      |                   |       |
    | cRED                 |           +----*-------*--+
    +----------------------+     ------+
#+END_SRC


  Example from http://doc.norang.ca/org-mode.html#playingwithditaa

