use utf8;
package Catapulse::Schema::Result::TS_Error;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Catapulse::Schema::Result::TS_Error

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<TS_error>

=cut

__PACKAGE__->table("TS_error");

=head1 ACCESSORS

=head2 error_time

  data_type: 'int'
  is_nullable: 0
  size: 11

=head2 jobid

  data_type: 'bigint'
  is_nullable: 0
  size: 20

=head2 message

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 funcid

  data_type: 'int'
  default_value: 0
  is_nullable: 0
  size: 11

=cut

__PACKAGE__->add_columns(
  "error_time",
  { data_type => "int", is_nullable => 0, size => 11 },
  "jobid",
  { data_type => "bigint", is_nullable => 0, size => 20 },
  "message",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "funcid",
  { data_type => "int", default_value => 0, is_nullable => 0, size => 11 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2018-08-05 19:35:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:iR3OuG/B+5LGbUsvFJggZg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
