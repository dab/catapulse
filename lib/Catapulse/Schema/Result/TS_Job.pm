use utf8;
package Catapulse::Schema::Result::TS_Job;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Catapulse::Schema::Result::TS_Job

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<TS_job>

=cut

__PACKAGE__->table("TS_job");

=head1 ACCESSORS

=head2 jobid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 funcid

  data_type: 'int'
  is_nullable: 0
  size: 11

=head2 arg

  data_type: 'mediumblob'
  is_nullable: 1

=head2 uniqkey

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 insert_time

  data_type: 'int'
  is_nullable: 1
  size: 11

=head2 run_after

  data_type: 'int'
  is_nullable: 0
  size: 11

=head2 grabbed_until

  data_type: 'int'
  is_nullable: 0
  size: 11

=head2 priority

  data_type: 'smallint'
  is_nullable: 1
  size: 6

=head2 coalesce

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=cut

__PACKAGE__->add_columns(
  "jobid",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "funcid",
  { data_type => "int", is_nullable => 0, size => 11 },
  "arg",
  { data_type => "mediumblob", is_nullable => 1 },
  "uniqkey",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "insert_time",
  { data_type => "int", is_nullable => 1, size => 11 },
  "run_after",
  { data_type => "int", is_nullable => 0, size => 11 },
  "grabbed_until",
  { data_type => "int", is_nullable => 0, size => 11 },
  "priority",
  { data_type => "smallint", is_nullable => 1, size => 6 },
  "coalesce",
  { data_type => "varchar", is_nullable => 1, size => 255 },
);

=head1 PRIMARY KEY

=over 4

=item * L</jobid>

=back

=cut

__PACKAGE__->set_primary_key("jobid");

=head1 UNIQUE CONSTRAINTS

=head2 C<funcid_uniqkey_unique>

=over 4

=item * L</funcid>

=item * L</uniqkey>

=back

=cut

__PACKAGE__->add_unique_constraint("funcid_uniqkey_unique", ["funcid", "uniqkey"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2018-08-05 19:35:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ho4G//WF5Qpr7CsHMa5ZgA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
