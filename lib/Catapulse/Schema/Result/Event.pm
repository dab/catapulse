use utf8;
package Catapulse::Schema::Result::Event;

=head1 NAME

Catapulse::Schema::Result::Event

=cut

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
use HTML::Entities;

extends 'DBIx::Class::Core';


__PACKAGE__->load_components(
    qw/ TimeStamp  DateTime::Epoch InflateColumn::DateTime/);

__PACKAGE__->table("event");
__PACKAGE__->add_columns(
    "id",
    {
        data_type         => "INTEGER",
        is_nullable       => 0,
        size              => undef,
        is_auto_increment => 1
    },
    "user_id",
    { data_type => "INTEGER", is_nullable => 0, size => undef },
    "created",
    { data_type => "datetime", is_nullable => 0, set_on_create => 1},
    "modified",
    { data_type => "datetime", is_nullable => 1 },
    "start",
    { data_type => "varchar", inflate_datetime => 'epoch', is_nullable => 0 },
    "end",
    { data_type => "varchar", inflate_datetime => 'epoch', is_nullable => 0 },
    "title",
    { data_type => "TEXT", is_nullable => 0, size => undef },
    "description",
    { data_type => "TEXT", is_nullable => 0, size => undef },
);

__PACKAGE__->set_primary_key("id");


__PACKAGE__->belongs_to(
    "user",
    "Catapulse::Schema::Result::User",
    { id => "user_id" }
);



=head1 NAME

Catapulse::Schema::Result::Event - store events

=head1 METHODS

=head2 TO_JSON

=cut

sub TO_JSON {
    my $self = shift;

    my $res =  { id          => $self->id,
		 start       => $self->start->ymd . " " . $self->start->hms,
		 end         => $self->end->ymd   . " " . $self->end->hms,
		 title       => $self->title,
		 description => $self->description,
              };
    utf8::decode($res->{title});
    utf8::decode($res->{description});
    return $res;
}


=head1 AUTHOR

Daniel Brosseau <dab@catapulse.org>

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
