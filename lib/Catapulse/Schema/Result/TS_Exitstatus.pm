use utf8;
package Catapulse::Schema::Result::TS_Exitstatus;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Catapulse::Schema::Result::TS_Exitstatus

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<TS_exitstatus>

=cut

__PACKAGE__->table("TS_exitstatus");

=head1 ACCESSORS

=head2 jobid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 funcid

  data_type: 'int'
  default_value: 0
  is_nullable: 0
  size: 11

=head2 status

  data_type: 'smallint'
  is_nullable: 1
  size: 6

=head2 completion_time

  data_type: 'int'
  is_nullable: 1
  size: 11

=head2 delete_after

  data_type: 'int'
  is_nullable: 1
  size: 11

=cut

__PACKAGE__->add_columns(
  "jobid",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "funcid",
  { data_type => "int", default_value => 0, is_nullable => 0, size => 11 },
  "status",
  { data_type => "smallint", is_nullable => 1, size => 6 },
  "completion_time",
  { data_type => "int", is_nullable => 1, size => 11 },
  "delete_after",
  { data_type => "int", is_nullable => 1, size => 11 },
);

=head1 PRIMARY KEY

=over 4

=item * L</jobid>

=back

=cut

__PACKAGE__->set_primary_key("jobid");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2018-08-05 19:35:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Y1L0H4OUJ3VXAkdqW5FfVA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
