use utf8;
package Catapulse::Schema::Result::TS_Funcmap;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Catapulse::Schema::Result::TS_Funcmap

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<TS_funcmap>

=cut

__PACKAGE__->table("TS_funcmap");

=head1 ACCESSORS

=head2 funcid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 funcname

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=cut

__PACKAGE__->add_columns(
  "funcid",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "funcname",
  { data_type => "varchar", is_nullable => 0, size => 255 },
);

=head1 PRIMARY KEY

=over 4

=item * L</funcid>

=back

=cut

__PACKAGE__->set_primary_key("funcid");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2018-08-05 19:35:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:q/f3fhCXNag13tdPACv0RQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
