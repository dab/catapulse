package Catapulse::XUtils;

use strict;
use warnings;


use FindBin qw($Bin);
use lib "$Bin/../lib";
use TheSchwartz::Moosified;     # theschwartz
use YAML ();                    # config
use Path::Class::File;
use File::Spec;
use DBI;
use Catapulse::Schema;

use base 'Exporter';
use vars qw/@EXPORT_OK $base_path $config $cache $tt2 $theschwartz $home/;
@EXPORT_OK = qw/
                 config
                 theschwartz
                 home
                 upload_dir
                 upload_url
                 schema
                 dsn
                 sqlitefile
                 persistent_dir
               /;


my $self = Path::Class::File->new( File::Spec->rel2abs( $0 ) );
my $dsn;


sub dsn {
    my $conf = shift;

    my $config = config($conf);
    $dsn  = $config->{'Model::DBIC'}->{'connect_info'}->{dsn};
    $dsn =~ s/__HOME__/$FindBin::Bin\/\.\./g;
    $dsn =~ s/__path_to\(([^)]+)\)__/ _path_to($1)  /e;

    return $dsn;
}

sub sqlitefile{
    my $conf = shift;

    die "The name of the config file is missing\n" if ( ! $conf);
    my $dsn = dsn($conf);
    if ( $dsn =~ /SQLite/ ) {
        my (undef,undef,$sqlitefile) = split ':', $dsn;
        return $sqlitefile;
    }

}

sub schema{
    my $fileconfig = shift;

    my ($user, $password, $unicode_option);
    $config = config($fileconfig);
    
    eval {
        if (!$dsn) {
     
            if (ref $config->{'Model::DBIC'}->{'connect_info'}) {
                $dsn  = $config->{'Model::DBIC'}->{'connect_info'}->{dsn};
                $user = $config->{'Model::DBIC'}->{'connect_info'}->{user};
                $password = $config->{'Model::DBIC'}->{'connect_info'}->{password};

                # Determine database type amongst: SQLite, Pg or MySQL
                my $db_type = lc($dsn =~ m/^dbi:(\w+)/);
                my %unicode_connection_for_db = (
                                                 'sqlite' => { sqlite_unicode    => 1 },
                                                 'pg'     => { pg_enable_utf8    => 1 },
                                                 'mysql'  => { mysql_enable_utf8 => 1 },
                                                );
                $unicode_option = $unicode_connection_for_db{$db_type};
            } else {
                $dsn = $config->{'Model::DBIC'}->{'connect_info'};
            }
        }
    };
    if ($@) {
        die "Your DSN line in catapulse.conf doesn't look like a valid DSN."
          . "  Add one, or pass it on the command line.";
    }
    die "No valid Data Source Name (DSN).\n" if !$dsn;
    $dsn =~ s/__HOME__/$FindBin::Bin\/\.\./g;
    $dsn =~ s/__path_to\(([^)]+)\)__/ _path_to($1)  /e;

    my $schema = Catapulse::Schema->connect($dsn, $user, $password, $unicode_option)
      or die "Failed to connect to database";

    return $schema;
}

sub persistent_dir {
    my $cfgfile = shift;

    my $config = YAML::LoadFile($cfgfile);
    my $injectconf = $config->{'CatalystX::InjectModule'};

    return $injectconf->{persistent_dir};

}

sub upload_url {
    return "/static/uploads";
}

sub upload_dir {
    my $app_root_dir = home() . "/root";
    my $upload_url= upload_url();
    return "$app_root_dir/$upload_url";

}

sub home {
    return $home if ($home);


    if ( ! $ENV{CATALYST_HOME}  ){
        chomp( $home = `pwd` );
        $ENV{CATALYST_HOME} = $home;
        warn "CATALYST_HOME is not defined, I use 'pwd'\nCATALYST_HOME=$home";
    }

    $home = $ENV{CATALYST_HOME};
    return $home;
}

sub config {
    my $conffile = shift;

    $conffile ||= $ENV{CATALYST_CONFIG};
    $conffile ||= "catapulse_web.yml";
    my $home = home();

    my $conf = "$home/$conffile";
    if ( ! -e $conf ){
        warn "Conf File does not exist, it must be relatif to CATALYST_HOME ( CATALYST_HOME=$home config file = $conffile )";
    }

    my $conf_local = $conf;
    $conf_local =~ s/\.yml$//;
    $conf_local .= "_local.yml";
    my $config_file       = Path::Class::File->new($conf );
    my $config_file_local = Path::Class::File->new($conf_local );

    my $config_data = YAML::LoadFile( $config_file );

    if ( -e $config_file_local ) {
        my $config_data_local = YAML::LoadFile( $config_file_local );
        $config = { %$config_data, %$config_data_local };
    } else {
        $config = { %$config_data };
    }

    return $config;
}

sub theschwartz {

    return $theschwartz if ($theschwartz);
    $config = config() unless ($config);

    die "config Theschwartz does not exist (perhaps you forgot to add \"TheSchwartz\"?"
      if ( ! defined $config->{TheSchwartz} );

    my $debug = $config->{TheSchwartz}->{debug} || 0;
    die "config Model::DBIC connect_info does not exist"
      if ( ! defined $config->{'Model::DBIC'}->{connect_info} );
 
    my $dsn = $config->{'Model::DBIC'}->{connect_info}->{dsn};
    $dsn =~ s/__path_to\(([^)]+)\)__/ _path_to($1)  /e;

    my $dbh = DBI->connect(
                           $dsn,
                           $config->{'Model::DBIC'}->{connect_info}->{user},
                           $config->{'Model::DBIC'}->{connect_info}->{password},
                           {
                            PrintError => 1, RaiseError => 1 }
                          );

    $theschwartz = TheSchwartz::Moosified->new( databases => [$dbh],
                                                prefix    => 'TS_',
                                                verbose   => $debug,
                                              );
    return $theschwartz;
}

sub _path_to {
    Path::Class::File->new(home(),+shift);
}

=head1 NAME

Catapulse::XUtils

=cut

=head1 SYNOPSIS

=head1 SUBROUTINES/METHODS

=head2 config

=head2 dsn

=head2 home

=head2 persistent_dir

=head2 schema

=head2 sqlitefile

=head2 theschwartz

=head2 upload_dir

=head2 upload_url


=head1 AUTHOR

Daniel Brosseau, C<< <dabd at catapulse.org> >>

=cut

1;
