package Catapulse::Web::Controller::Root;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catapulse::Web::ControllerBase' }
#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=encoding utf-8

=head1 NAME

Catapulse::Web::Controller::Root - Root Controller for Catapulse::Web

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 auto

run for all URLs (/)

=cut

sub auto :Private {
    my ($self, $c) = @_;

    # default css
    $c->assets->include("static/bootstrap-3.3.6/css/bootstrap.min.css");
    $c->assets->include("static/css/font-awesome.min.css");


    # # default js
    $c->assets->include("static/js/jquery-1.11.1.min.js");
    $c->assets->include("static/js/jquery-migrate-1.1.1.js");
    $c->assets->include("static/bootstrap-3.3.6/js/bootstrap.min.js");
    $c->assets->include("static/js/app.js");

    # # # used by toogle/edit/delete in lists
    $c->assets->include("static/js/jquery.livequery.js");
    $c->assets->include("static/js/jquery.pa.list_actions.js");

}

=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    # Default message
}

=head2 default

Standard 404 error page

=cut

sub default :Path {
    my ( $self, $c ) = @_;

    #$c->forward('not_found');
}

=head2 access_denied

access_denied page

=cut

sub access_denied :Path('access_denied') {
    my ( $self, $c ) = @_;
    $c->response->status(401);
}

=head2 not_found

Page not_found

=cut

sub not_found :Path('not_found') {
    my ( $self, $c ) = @_;
    $c->response->status(404);
    $c->stash->{'template'} = 'not_found.tt';
}

=head2 end

  Attempt to render a view, if needed.

  Calculate content of controller, blocks and build page

=cut


sub end : ActionClass('RenderView') {
    my ( $self, $c) = @_;

    return 1 if $c->req->method eq 'HEAD';
    return  if $c->response->status =~ /^(?:204|3\d\d)$/;
    return $c->res->output if ( $c->res->output && ! $c->stash->{template} );

}


=head1 AUTHOR

  dab,,,

=head1 LICENSE

  This library is free software. You can redistribute it and/or modify
    it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
