package Catapulse::Web::ControllerBase;

use Moose;
use namespace::clean -except => 'meta';

BEGIN {
    extends 'Catalyst::Controller';
    with qw(
               CatalystX::Component::Traits
               Catalyst::Component::ContextClosure
          );
}

=encoding utf-8

=head1 NAME

Catapulse::Web::ControllerBase - Base Controller for Catapulse::Web

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS



=head1 AUTHOR

  dab,,,

=head1 LICENSE

  This library is free software. You can redistribute it and/or modify
    it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
