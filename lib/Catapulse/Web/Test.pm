package Catapulse::Web::Test;



use Moose;
extends 'Test::WWW::Mechanize::Catalyst';

use Test::More;

has url => (
            is => 'rw',
            isa => 'Str',
            default => 'http://localhost:3000',
           );



sub login_ok{
    my $self  = shift;
    my $login = shift;
    my $pass  = shift;

    my $url = $self->url . '/login';
    $self->post_ok($url, [ username => $login, password => $login], "Logged in as $login");
}

sub get_ko{
    my $self = shift;
    my $url  = shift;
    my $msg  = shift;
    
    $self->get('$url');
    ok (! $self->success, $msg);
}

# sub post{
#   my $path  = shift;
#   my $args  = shift;

#   my ($res, $c) = ctx_request(POST $path, $args, Cookie => $cookie);
#   is( $res->code,'200', "POST $path, do 200");
#   return ($res, $c);
# }

# sub post_redirect{
#   my $path  = shift;
#   my $args  = shift;

#   my ($res, $c) = ctx_request(POST $path, $args, Cookie => $cookie);
#   is( $res->code,'302', "POST $path, do 302");
#   return ($res, $c);
# }

# sub is_denied {
#   my $path = shift;
#   is ( have_access($path), 0, "Access denied $path ");
# }

# sub is_allowed {
#   my $path = shift;
#   is ( have_access($path), 1, "Access allowed $path ");
# }

# sub is_not_found {
#   my $path = shift;
#   my ($res,undef) = ctx_request(GET "$path", Cookie => $cookie);
#   is( $res->code,'404', "$path, do 404");
# }


# # if code == 302 (redirect) and location == /access_denied => Denied
# # else Allowed
# sub have_access {
#   my $path = shift;
#   my ($res, $c) = ctx_request(GET "$u/$path", Cookie => $cookie);

#   return 0 if ( $res->code == 401 );
#   return 1;
#   # my $redirected = $res->code == 302 ? 1 : 0;
#   # my $loc_denied = (  defined $res->header('Location') &&
#   #                     $res->header('Location') eq '/access_denied' ? 1 : 0
#   #                  ) || 0;

#   # return ( $redirected && $loc_denied ) ? 0 : 1;
# }

=encoding utf-8

=head1 NAME

Catapulse::Web::Test - Catapulse::Web::Test

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 login_ok

=cut

=head2 get_ko

=cut


=head1 AUTHOR

  dab,,,

=head1 LICENSE

  This library is free software. You can redistribute it and/or modify
    it under the same terms as Perl itself.

=cut


1;
