package Catapulse::Web::View::TT;
use Moose;
use namespace::autoclean;

extends 'Catalyst::View::TT';

=head1 NAME

Catapulse::Web::View::TT - TT View for Catapulse

=head1 DESCRIPTION

TT View for Catapulse.

=head1 SEE ALSO

L<MyApp>

=head1 AUTHOR

dab,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
