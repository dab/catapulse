package FakeCatalystObject;

# See the difference among Dummy, Stub, Fake and Mock objects at
# http://xunitpatterns.com/Mocks,%20Fakes,%20Stubs%20and%20Dummies.html

use URI;

my $reverse;

my %prefs = (
    main_formatter => 'MojoMojo::Formatter::Markdown',
);

sub new {
    my $class = shift;
    bless {}, $class;
}

sub req {
    return $_[0];
}

sub res {
    return $_[0];
}

sub base {
    $_[0]->{path} ||= '/';
    return URI->new("http://example.com/");
}

sub reverse {
    return $reverse;
}

sub set_reverse {
    $reverse=$_[1];
}

sub stash {
    my $self = shift;
    return {
        page => $self,
        page_path => 'http://example.com/',
    };
}

sub flash {
    my $self = shift;
    return {
        page => $self,
        page_path => 'http://example.com/',
    };
}

sub path {
    my $self = shift;
    $path = $self->{path};
    return $path;
}

sub model {
    return $_[0];
}

sub result_source {
    return $_[0];
}

sub resultset {
    return $_[0];
}

sub ajax {}

sub action {
    return $_[0];
}

sub name { 'view' }

sub retrieve_pages_from_path {
    my ($self, $path) = @_;

    return;
}

sub path_pages {
    my ($self, $path) = @_;
    $path =~ s|^/||;
    if ($path =~ /existing/i && $path !~ /#new/) {
        my $page = FakeCatalystObject->new;
        $page->{path} = $path;
        return [$page], undef;
    } else {
        return [], [{path => $path}];
    }
}

sub cache {
    my ($self, $c) = @_;
    return undef;
}


sub redirect {
    my ($self, $url) = @_;
    $self->{url}=$url if $url;
    return $self->{url};
}

sub uri_for {
    my ($self, $url) = @_;
    return $url;
}

sub loc {
    my ($self, $text) = @_;
    return "Faking localization... $text ...fake complete.";
}

sub session {
    my ($self, $c) = @_;
    return '';
}

sub pref {
    my ($self, $c, $setting, $value) = @_;
    return '' if not defined $setting;
    return $prefs{$setting} || '' if not defined $value;
    $prefs{$setting} = $value;
}

sub sessionid {
    return 0;
}

=head1 NAME

FakeCatalystObject

=cut

=head1 SYNOPSIS

=head1 SUBROUTINES/METHODS


=head2 action

=head2 ajax

=head2 base

=head2 cache

=head2 flash

=head2 loc

=head2 model

=head2 name

=head2 new

=head2 path

=head2 path_pages

=head2 pref

=head2 redirect

=head2 req

=head2 res

=head2 result_source

=head2 resultset

=head2 retrieve_pages_from_path

=head2 reverse

=head2 session

=head2 sessionid

=head2 set_reverse

=head2 stash

=head2 uri_for


=head1 AUTHOR

Daniel Brosseau, C<< <dabd at catapulse.org> >>

=cut

1;
