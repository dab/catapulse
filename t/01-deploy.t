#!/usr/bin/env perl

use Test::Most;
use lib 't/lib';
use Catapulse::XUtils qw(schema);

BEGIN{
    $ENV{CATALYST_CONFIG} = 't/conf/cms.yml';
}

my $conf   = $ENV{CATALYST_CONFIG};
my $schema = schema($conf);

isa_ok($schema,'Catapulse::Schema');
$schema->deploy;
pass("deploy schema");
done_testing;
