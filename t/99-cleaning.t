#!/usr/bin/env perl

use Test::Most;
use lib 't/lib';
use Catapulse::XUtils qw(schema sqlitefile persistent_dir sqlitefile);

my $conf   = 't/conf/cms.yml';

my $sqlitefile = sqlitefile($conf);
if ( -e $sqlitefile ) {
    ok( unlink $sqlitefile, 0 );
} else {
    fail("$sqlitefile does not exist");
}

my $persistent_dir = persistent_dir($conf);
ok(unlink <$persistent_dir/*.yml>,0);

done_testing;
