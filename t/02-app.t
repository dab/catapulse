#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;

BEGIN{
    $ENV{CATALYST_CONFIG} = 't/conf/cms.yml';
}

use Catalyst::Test 'Catapulse::Web';

ok( request('/')->is_success, 'Request / succeed' );
ok( request('/test')->is_success, 'Request /test succeed (from installed Module)' );

done_testing();

END { unlink "t/conf/cms_local.yml" }
