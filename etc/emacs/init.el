(require 'package)
(defvar initial-directory (getenv "PWD")) 
(setq package-user-dir (concat initial-directory "/etc/emacs/elpa"))

(setq package-enable-at-startup nil)
(setq package-archives '(("org"       . "http://orgmode.org/elpa/")
                         ("gnu"       . "http://elpa.gnu.org/packages/")
                         ("melpa"     . "http://melpa.org/packages/")))
(package-initialize)

(unless (package-installed-p 'use-package) ; unless it is already installed
  (package-refresh-contents) ; updage packages archive
  (package-install 'use-package)) ; and install the most recent version of use-package

(require 'use-package) ; guess what this one does too ?

(use-package htmlize
  :ensure t)

(require 'org)

(setq org-ditaa-jar-path (concat initial-directory "/etc/emacs/src/ditaa0_6b.jar"))
(setq org-plantuml-jar-path (concat initial-directory "/etc/emacs/src/plantuml.jar"))

(org-babel-do-load-languages
 (quote org-babel-load-languages)
 (quote ((emacs-lisp . t)
         (dot . t)
         (ditaa . t)
         (python . t)
         (ruby . t)
         (gnuplot . t)
         (shell . t)
         (perl . t)
         (org . t)
         (plantuml . t)
         (latex . t))))

(setq org-confirm-babel-evaluate nil)

;; (use-package solarized-theme
;;   :ensure t)

;; (setq org-html-htmlize-output-type 'css)

;; (defun my/with-theme (theme fn &rest args)
;;   (let ((current-themes custom-enabled-themes))
;;     (mapcar #'disable-theme custom-enabled-themes)
;;     (load-theme theme t)
;;     (let ((result (apply fn args)))
;;       (mapcar #'disable-theme custom-enabled-themes)
;;       (mapcar (lambda (theme) (load-theme theme t)) current-themes)
;;       result)))

;; (advice-add #'org-export-to-file :around (apply-partially #'my/with-theme 'solarized-dark))
;; (advice-add #'org-export-to-buffer :around (apply-partially #'my/with-theme 'solarized-dark))


;;(message package-user-dir)
